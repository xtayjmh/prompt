﻿//提交ajax地址
var ajaxUrl = 'http://192.168.1.25:8080';
//ajax请求 兼容IE
jQuery.support.cors = true;//ie9以下兼容

//trim() 兼容IE
if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
}

// var ajaxUrl = 'http://www.sosoapi.com/pass/mock/8904';

var intMax = 2147483647;

var roomType;

var rolName = '';
// var currentUserRole = $.cookie('role');
var currentUserRole = $.cookie('loginName');
if (currentUserRole != '') {
    rolName = currentUserRole
} else {
    rolName = "管理员";
}

//侧边栏菜单
function activeMenu(value) {
    $('#main-menu span.menu-name').each(function (i, item) {
        if ($(item).text() == value) {
            $(item).parent().addClass('curr');
            $(item).parent().parent().parent().parent().addClass('active');
        }
    });
    $('#main-menu').metisMenu();
    var role = $.cookie('role');
    var roleList = [
        {"id": "roleManage", "value": "后台权限管理"},
        {"id": "shopManage", "value": "店铺管理"},
        {"id": "shopPersonManage", "value": "店铺关联人员管理"},
        {"id": "shopDataManage", "value": "店铺关联数据管理"},
        {"id": "shopReportManage", "value": "店铺关联报表管理"},
        {"id": "feedbackManage", "value": "用户反馈回复"},
        {"id": "systemLogUpdateManage", "value": "系统日志与更新版本管理"},
        {"id": "systemSetting", "value": "系统设置"},
    ];
    roleList.forEach(function (item) {
        if (role.indexOf(item.value) == -1) {
            $("#" + item.id).hide();
        }
    })
    $("#feedbackManage").hide();
}

function getRoomType() {
    $.ajax({
        type: "get",
        dataType: "json",
        data: '',
        url: ajaxUrl + '/dormitory/type/query',
        success: function (data) {
            if (data.resultCode == 'SUCCESS') {
                roomType = eval(data.resultMsg);
            } else {
            }
        }
    });
}

//限制文本框字数
function limit(id, txtNum) {
    var curLength = $(id).val().length;
    if (curLength > txtNum) {
        var num = $(id).val().substr(0, txtNum);
        $(id).val(num);
        $('#parsley-text > li').addClass('error-text').html('超出字数限制');
        $(id).addClass('parsley-error');
    }
    else {
        $(id).removeClass('parsley-error');
        $('#parsley-text > li').removeClass('error-text').html('剩余<b><span id="textCount" class="error-text"></span></b>个字');
        $("#textCount").text(txtNum - $(id).val().length);
    }
}

//学生信息
function studentModal(code) {
    $('#myModal').remove();
    $.ajax({
        method: 'get',
        url: ajaxUrl + '/student/query/by/code',
        dataType: 'json',
        cache: false,
        data: {
            'studentCode': code
        },
        success: function (data) {
            if (data.resultCode == 'SUCCESS') {
                var rs = JSON.parse(data.resultMsg);
                var sex = rs.sex;
                // switch (rs.sex){
                //     case 0:
                //         sex='女';
                //     break;
                //     default:
                //         sex='男';
                //     break;
                // }
                console.log(rs);
                var div = '<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><spanaria-hidden="true">&times;</span></button> <h4 class="modal-title">学生信息</h4> </div> <form class="form-horizontal form-label-left"> <div class="modal-body"> ' +
                    '<div class="form-group">' +
                    ' <label for="code" class="col-sm-3 control-label">学号</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="code" value="' + rs.code + '" type="text" class="form-control"/></div>' +
                    ' </div> ' +
                    '<div class="form-group">' +
                    ' <label for="name" class="col-sm-3 control-label">姓名</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="name" value="' + rs.name + '" type="text" class="form-control"/></div>' +
                    ' </div> ' +
                    '<div class="form-group">' +
                    ' <label for="name" class="col-sm-3 control-label">性别</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" class="form-control" type="text" name="sex" value="' + sex + '" ></div></div> ' +
                    '<div class="form-group">' +
                    ' <label for="dorm" class="col-sm-3 control-label">寝室</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="dorm" value="' + rs.dormInfo + '" type="text" class="form-control"/></div> ' +
                    '</div> ' +
                    '<div class="form-group"> ' +
                    '<label for="nativePlace" class="col-sm-3 control-label">籍贯</label>' +
                    ' <div class="col-sm-8"><input disabled="disabled" id="nativePlace" value="' + rs.nativePlace + '" type="text" class="form-control"/></div> ' +
                    '</div>' +
                    ' <div class="form-group"> ' +
                    '<label for="mobile" class="col-sm-3 control-label">手机号</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="mobile" value="' + rs.mobile + '" type="text" class="form-control"/></div>' +
                    ' </div> ' +
                    '<div class="form-group"> ' +
                    '<label for="birthDate" class="col-sm-3 control-label">出生日期</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="birthDate" value="' + rs.birthDate + '" type="text" class="form-control"/></div>' +
                    ' </div> ' +
                    '<div class="form-group"> ' +
                    '<label for="cardNumber" class="col-sm-3 control-label">身份证号</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="cardNumber" value="' + rs.cardNumber + '" type="text" class="form-control"/></div> ' +
                    '</div>' +
                    ' <div class="form-group"> ' +
                    '<label for="enrollDate" class="col-sm-3 control-label">入学日期</label>' +
                    ' <div class="col-sm-8"><input disabled="disabled" id="enrollDate" value="' + rs.enrollDate + '" type="text" class="form-control"/></div>' +
                    ' </div> ' +
                    '<div class="form-group">' +
                    ' <label for="schoolState" class="col-sm-3 control-label">在校状态</label>' +
                    ' <div class="col-sm-8"><input disabled="disabled" id="schoolState" value="' + rs.schoolState + '" type="text" class="form-control"/></div> ' +
                    '</div> ' +
                    '<div class="form-group"> ' +
                    '<label for="politicalStatus" class="col-sm-3 control-label">政治面貌</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="politicalStatus" value="' + rs.politicalStatus + '" type="text" class="form-control"/></div> ' +
                    '</div> ' +
                    '<div class="form-group"> ' +
                    '<label for="dadMobile" class="col-sm-3 control-label">父亲手机</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="dadMobile" value="' + rs.dadMobile + '" type="text" class="form-control"/></div> ' +
                    '</div> ' +
                    '<div class="form-group"> ' +
                    '<label for="mamMobile" class="col-sm-3 control-label">母亲手机</label> ' +
                    '<div class="col-sm-8"><input disabled="disabled" id="mamMobile" value="' + rs.mamMobile + '" type="text" class="form-control"/></div>' +
                    ' </div> ' +
                    '<div class="form-group"> ' +
                    '<label for="homeAddress" class="col-sm-3 control-label">家庭住址</label>' +
                    ' <div class="col-sm-8"><input disabled="disabled" id="homeAddress" value="' + rs.homeAddress + '" type="text" class="form-control"/></div> ' +
                    '</div> ' +
                    '</div> <div class="modal-footer"> <button type="button" class="btn btn-gray" data-dismiss="modal">关闭</button> </div> </form> </div> </div> </div>';
                $(document.body).append(div);
                $('#myModal').modal('show');
            }
            else {
                alert('请求参数错误，学号不存在！');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
//                    $('#stuCode').val('');
//                    $('#stuName').val('');
            $('#parsley-id-3').text('没有找到学生信息，请重新输入...');
            closeLoading();
        }
    })
}


//截取时间
function getdate(ns) {
    var date = new Date(ns),
        y = date.getFullYear(ns),
        m = date.getMonth(ns) + 1,
        d = date.getDate(ns);
    return y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d)  /*+ " "+ date.toTimeString().substr(0, 8)*/;
}


// function getdate(ns) {
//     ns.toString();
//     console.log(ns);
//     return ns;
// }


//窗口变化
$(window).bind("load resize", function () {
    $('.navbar-side').css('height', 'auto');
    if ($(this).width() < 768) {
        $('.navbar-side').addClass('collapse');
    } else {
        $('.navbar-side').removeClass('collapse');
    }
});


//定义变量
var currentUserName = $.cookie('loginName');
var currentPw = $.cookie('pw');
var currentUserRole = $.cookie('role');

//session判断
// if (currentUserName && currentUserName != '' && currentPw && currentPw != "") {
//     $.ajax({
//         method: 'get',
//         url: ajaxUrl + '/user/login/status',//'http://121.22.36.190:8089/tx/user/check'
//         dataType: 'json',
//         timeout: 10000,
//         async: false,
//         cache: false,
//         data: {
//             'username': currentUserName,
//             'password': currentPw
//         },
//         success: function (data) {
//             if (data.resultCode != 'SUCCESS' || data.resultMsg != '用户登录中') {
//                // alert('账户已过期，请重新登录');
//                 window.location.href = 'login.html';
//             }
//         },
//         error: function (jqXHR, textStatus, errorThrown) {
//            // alert('账户已过期，请重新登录');
//            window.location.href = 'login.html';
//         }
//     });
// }
// else {
//     //alert('账户已过期，请重新登录');
//     window.location.href = 'login.html';
// }

function openLoadingModal(id) {
    $(id).append('<div class="loadingboxer"><div class="loadingbg"></div><div class="loadingmask"></div></div>');
}

//开始loading
function openLoading() {
    $(document.body).append('<div class="loadingboxer"><div class="loadingbg"></div><div class="loadingmask"></div></div>');
}

//关闭loading
function closeLoading() {
    $('.loadingboxer').remove();
}

//自定义分页
function customPaging(pageNumber, pageSize, dataTotal) {
    var pageTotal = Math.ceil(dataTotal / pageSize);
    var page = page;
    var result = '<div class="page-info">';
    if (dataTotal != 0) {
        result += '当前显示' + ((pageNumber - 1) * pageSize + 1) + '-' + ((pageNumber * pageSize > dataTotal) ? dataTotal : pageNumber * pageSize) + '条&nbsp;&nbsp;'
        result += '共' + dataTotal + '条数据'
        result += '</div>';
        result += '<div class="page-number">';
    } else {
        result += '当前共' + dataTotal + '条数据'
        result += '</div>';
        result += '<div class="page-number">';
    }


    //上一页
    if (pageNumber == 1) {
        result += '<span>上一页</span>';
    }
    else {
        result += '<a href="javascript:void(0)" onclick="createTable(' + (pageNumber - 1) + ')">上一页</a>';
    }

    //显示数字
    if (pageTotal < 8) {//少于8页
        for (var i = 0; i < pageTotal; i++) {
            if (i + 1 == pageNumber) {
                result += '<span class="curr">' + (i + 1) + '</span>';
            }
            else {
                result += '<a href="javascript:void(0)" onclick="createTable(' + (i + 1) + ')">' + (i + 1) + '</a>';
            }
        }
    }
    else {
        if (pageNumber < 5) {//前5页
            for (var i = 0; i < 5; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="createTable(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="createTable(' + pageTotal + ')">' + pageTotal + '</a>';
        }
        else if (pageNumber > pageTotal - 4) {//后5页
            result += '<a href="javascript:void(0)" onclick="createTable(1)">1</a>';
            result += '<span>...</span>';
            for (var i = pageTotal - 5; i < pageTotal; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="createTable(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
        }
        else {//中间3页
            result += '<a href="javascript:void(0)" onclick="createTable(1)">1</a>';
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="createTable(' + (pageNumber - 1) + ')">' + (pageNumber - 1) + '</a>';
            result += '<span class="curr">' + pageNumber + '</span>';
            result += '<a href="javascript:void(0)" onclick="createTable(' + (pageNumber + 1) + ')">' + (pageNumber + 1) + '</a>';
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="createTable(' + pageTotal + ')">' + pageTotal + '</a>';
        }
    }

    //下一页
    if (pageNumber == pageTotal) {
        result += '<span>下一页</span>';
    }
    else {
        result += '<a href="javascript:void(0)" onclick="createTable(' + (pageNumber + 1) + ')">下一页</a>';
    }

    result += '</div>';

    return result;
}

//自定义 搜索 分页
function searchPaging(pageNumber, pageSize, dataTotal, page) {
    var pageTotal = Math.ceil(dataTotal / pageSize);
    var page = page;
    var result = '<div class="page-info">';
    result += '当前显示' + ((pageNumber - 1) * pageSize + 1) + '-' + ((pageNumber * pageSize > dataTotal) ? dataTotal : pageNumber * pageSize) + '条&nbsp;&nbsp;'
    result += '共' + dataTotal + '条数据'
    result += '</div>';
    result += '<div class="page-number">';

    //上一页
    if (pageNumber == 1) {
        result += '<span>上一页</span>';
    }
    else {
        result += '<a href="javascript:void(0)" onclick="search(' + (pageNumber - 1) + ')">上一页</a>';
    }

    //显示数字
    if (pageTotal < 8) {//少于8页
        for (var i = 0; i < pageTotal; i++) {
            if (i + 1 == pageNumber) {
                result += '<span class="curr">' + (i + 1) + '</span>';
            }
            else {
                result += '<a href="javascript:void(0)" onclick="search(' + (i + 1) + ')">' + (i + 1) + '</a>';
            }
        }
    }
    else {
        if (pageNumber < 5) {//前5页
            for (var i = 0; i < 5; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="search(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="search(' + pageTotal + ')">' + pageTotal + '</a>';
        }
        else if (pageNumber > pageTotal - 4) {//后5页
            result += '<a href="javascript:void(0)" onclick="search(1)">1</a>';
            result += '<span>...</span>';
            for (var i = pageTotal - 5; i < pageTotal; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="search(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
        }
        else {//中间3页
            result += '<a href="javascript:void(0)" onclick="search(1)">1</a>';
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="search(' + (pageNumber - 1) + ')">' + (pageNumber - 1) + '</a>';
            result += '<span class="curr">' + pageNumber + '</span>';
            result += '<a href="javascript:void(0)" onclick="search(' + (pageNumber + 1) + ')">' + (pageNumber + 1) + '</a>';
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="search(' + pageTotal + ')">' + pageTotal + '</a>';
        }
    }

    //下一页
    if (pageNumber == pageTotal) {
        result += '<span>下一页</span>';
    }
    else {
        result += '<a href="javascript:void(0)" onclick="search(' + (pageNumber + 1) + ')">下一页</a>';
    }

    result += '</div>';

    return result;
}

//选择学生分页
function searchStudentsTab(pageNumber, pageSize, dataTotal, page) {
    var pageTotal = Math.ceil(dataTotal / pageSize);
    var page = page;
    var result = '<div class="page-info">';
    result += '当前显示' + ((pageNumber - 1) * pageSize + 1) + '-' + ((pageNumber * pageSize > dataTotal) ? dataTotal : pageNumber * pageSize) + '条&nbsp;&nbsp;'
    result += '共' + dataTotal + '条数据'
    result += '</div>';
    result += '<div class="page-number">';

    //上一页
    if (pageNumber == 1) {
        result += '<span>上一页</span>';
    }
    else {
        result += '<a href="javascript:void(0)" onclick="searchStudents(' + (pageNumber - 1) + ')">上一页</a>';
    }

    //显示数字
    if (pageTotal < 8) {//少于8页
        for (var i = 0; i < pageTotal; i++) {
            if (i + 1 == pageNumber) {
                result += '<span class="curr">' + (i + 1) + '</span>';
            }
            else {
                result += '<a href="javascript:void(0)" onclick="searchStudents(' + (i + 1) + ')">' + (i + 1) + '</a>';
            }
        }
    }
    else {
        if (pageNumber < 5) {//前5页
            for (var i = 0; i < 5; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="searchStudents(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="searchStudents(' + pageTotal + ')">' + pageTotal + '</a>';
        }
        else if (pageNumber > pageTotal - 4) {//后5页
            result += '<a href="javascript:void(0)" onclick="searchStudents(1)">1</a>';
            result += '<span>...</span>';
            for (var i = pageTotal - 5; i < pageTotal; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="searchStudents(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
        }
        else {//中间3页
            result += '<a href="javascript:void(0)" onclick="searchStudents(1)">1</a>';
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="searchStudents(' + (pageNumber - 1) + ')">' + (pageNumber - 1) + '</a>';
            result += '<span class="curr">' + pageNumber + '</span>';
            result += '<a href="javascript:void(0)" onclick="searchStudents(' + (pageNumber + 1) + ')">' + (pageNumber + 1) + '</a>';
            result += '<span>...</span>';
            result += '<a href="javascript:void(0)" onclick="searchStudents(' + pageTotal + ')">' + pageTotal + '</a>';
        }
    }

    //下一页
    if (pageNumber == pageTotal) {
        result += '<span>下一页</span>';
    }
    else {
        result += '<a href="javascript:void(0)" onclick="searchStudents(' + (pageNumber + 1) + ')">下一页</a>';
    }

    result += '</div>';

    return result;
}

//echarts 柱状图
function echartsBar(target, titleMain, titleSub, unit, seriesName, seriesValue) {
    $(target).show().css('height', '580px');

    //是否倾斜
    var angleX = seriesName.length > 8 ? 60 : 0;

    //echarts配置
    var option = {
        title: {
            text: titleMain,
            subtext: titleSub
        },
        tooltip: {
            trigger: 'item',
            formatter: "{b}<br /><b>{c}</b> " + unit
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: true},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        grid: {
            y2: 140
        },
        xAxis: [
            {
                type: 'category',
                data: seriesName,
                axisLabel: {
                    interval: 0, //横轴信息全部显示
                    rotate: angleX //倾斜角度
                }
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Acutal',
                type: "bar",
                itemStyle: {
                    normal: {
                        color: function (params) {
                            // build a color map as your need.
                            var colorList = [
                                '#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B',
                                '#FE8463', '#9BCA63', '#FAD860', '#F3A43B', '#60C0DD',
                                '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#26C0C0',
                                '#C1343B', '#B5C445', '#FCCE32', '#E57C25', '#37537B',
                                '#FEB4A3', '#9B6AB3', '#FAB3C0', '#F4B53B', '#56B0DD',
                                '#D76BAB', '#CA6B79', '#F47B31', '#F6B45A', '#236AC0'
                            ];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{c} ' + unit
                        }
                    }
                },
                data: seriesValue
            }
        ]
    };

    // 为echarts对象加载数据
    var myChart = echarts.init($(target)[0], 'macarons');
    myChart.setOption(option);

    //重置容器高宽
    $(window).resize(function () {
        $(target).css('width', $(target).parent().width() + 'px');
        myChart.resize();
    });
}

//echarts 柱状图 -- 横向显示
function echartsBar2(target, titleMain, titleSub, unit, seriesName, seriesValue) {
    //计算有几行
    var sw = seriesName.length * 20 + 300;
    $(target).show().css('height', sw + 'px');

    //echarts配置
    var option = {
        title: {
            text: titleMain,
            subtext: titleSub
        },
        tooltip: {
            trigger: 'item',
            formatter: "{b}<br /><b>{c}</b> " + unit
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: true},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        xAxis: [
            {
                type: 'value',
                axisLabel: {
                    interval: 0 //横轴信息全部显示
                }
            }
        ],
        yAxis: [
            {
                type: 'category',
                data: seriesName
            }
        ],
        series: [
            {
                name: 'Acutal',
                type: "bar",
                itemStyle: {
                    normal: {
                        color: function (params) {
                            // build a color map as your need.
                            var colorList = [
                                '#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B',
                                '#FE8463', '#9BCA63', '#FAD860', '#F3A43B', '#60C0DD',
                                '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#26C0C0',
                                '#C1343B', '#B5C445', '#FCCE32', '#E57C25', '#37537B',
                                '#FEB4A3', '#9B6AB3', '#FAB3C0', '#F4B53B', '#56B0DD',
                                '#D76BAB', '#CA6B79', '#F47B31', '#F6B45A', '#236AC0'
                            ];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            show: true,
                            position: 'right',
                            formatter: '{c} ' + unit
                        }
                    }
                },
                data: seriesValue
            }
        ]
    };

    // 为echarts对象加载数据
    var myChart = echarts.init($(target)[0], 'macarons');
    myChart.setOption(option);

    //重置容器高宽
    $(window).resize(function () {
        $(target).css('width', $(target).parent().width() + 'px');
        myChart.resize();
    });
}

//echarts 柱状图 -- 温度计
function echartsBar3(target, titleMain, titleSub, unit, seriesName, seriesValue, seriesDiff) {
    //计算有几行
    var sw = seriesName.length * 60 + 600;
    $(target).show().css('height', sw + 'px');

    //echarts配置
    var option = {
        title: {
            text: titleMain,
            subtext: titleSub
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'none'
            },
            formatter: function (params) {
                return params[0].name + '<br/>'
                    + params[0].seriesName + ' : ' + params[0].value + ' ' + unit + '<br/>'
                    + params[1].seriesName + ' : ' + (params[1].value + params[0].value) + ' ' + unit;
            }
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: true},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        xAxis: [
            {
                type: 'value',
                axisLabel: {
                    interval: 0 //横轴信息全部显示
                }
            }
        ],
        yAxis: [
            {
                type: 'category',
                data: seriesName
            }
        ],
        series: [
            {
                name: '实际',
                type: "bar",
                stack: 'sum',
                barCategoryGap: '50%',
                itemStyle: {
                    normal: {
                        color: function (params) {
                            // build a color map as your need.
                            var colorList = [
                                '#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B',
                                '#FE8463', '#9BCA63', '#FAD860', '#F3A43B', '#60C0DD',
                                '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#26C0C0',
                                '#C1343B', '#B5C445', '#FCCE32', '#E57C25', '#37537B',
                                '#FEB4A3', '#9B6AB3', '#FAB3C0', '#F4B53B', '#56B0DD',
                                '#D76BAB', '#CA6B79', '#F47B31', '#F6B45A', '#236AC0'
                            ];
                            return colorList[params.dataIndex]
                        },
                        barBorderColor: 'tomato',
                        barBorderWidth: 0,
                        barBorderRadius: 0,
                        label: {
                            show: true,
                            position: 'insideRight',
                            formatter: '{c} ' + unit
                        }
                    }
                },
                data: seriesValue
            },
            {
                name: '总数',
                type: 'bar',
                stack: 'sum',
                itemStyle: {
                    normal: {
                        color: '#fff',
                        barBorderColor: 'tomato',
                        barBorderWidth: 3,
                        barBorderRadius: 0,
                        label: {
                            show: true,
                            position: 'right',
                            formatter: function (params) {
                                for (var i = 0; i < option.yAxis[0].data.length; i++) {
                                    if (option.yAxis[0].data[i] == params.name) {
                                        return (option.series[0].data[i].value + params.value) + ' ' + unit;
                                    }
                                }
                            },
                            textStyle: {
                                color: 'tomato'
                            }
                        }
                    }
                },
                data: seriesDiff
            }
        ]
    };

    // 为echarts对象加载数据
    var myChart = echarts.init($(target)[0], 'macarons');
    myChart.setOption(option);

    //重置容器高宽
    $(window).resize(function () {
        $(target).css('width', $(target).parent().width() + 'px');
        myChart.resize();
    });
}

//echarts 饼状图
function echartsPie(target, titleMain, titleSub, unit, seriesName, seriesValue, minValue) {
    //计算图例有几行
    var tw = $(target).parent().width();
    var sw = seriesName.length * 100;
    var line = sw / tw + 1;
    var th = 400 + line * 20;

    // $(target).show().css('height', th + 'px');
    $(target).show().css('height', '5800px');
    //图例位置
    var legendY = titleSub != '' ? 50 : 25;

    //饼状图位置
    var centerY = line * 20 + 210;

    //echarts配置
    var option = {
        title: {
            text: titleMain,
            subtext: titleSub
        },
        tooltip: {
            trigger: 'item',
            formatter: "{b}<br /><b>{c}</b> " + unit
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: true},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        legend: {
            orient: 'horizontal', //horizontal|vertical
            x: 'center', //center|left|right|{number}
            y: legendY, //top|center|bottom {number}
            data: seriesName
        },
        series: [
            {
                name: 'Acutal',
                type: 'pie',
                radius: 150,
                center: ['50%', centerY],
                itemStyle: {
                    normal: {
                        borderColor: '#fff',
                        borderWidth: 1,
                        label: {
                            position: 'inside',
                            formatter: function (params, ticket, callback) {
                                //值大于minValue，图中显示
                                if (params.value >= minValue) {
                                    return params.name;
                                }
                                else {
                                    return '';
                                }
                            },
                            show: function () {
                                return false;
                            }
                        }
                    },
                    emphasis: {
                        borderColor: 'red',
                        borderWidth: 5,
                        label: {
                            show: true,
                            formatter: '{b}: {c} ' + unit,
                            textStyle: {
                                fontSize: 16
                            }
                        }
                    }
                },
                data: seriesValue
            }
        ]
    };

    // 为echarts对象加载数据
    var myChart = echarts.init($(target)[0], 'macarons');
    myChart.setOption(option);

    //重置容器高宽
    $(window).resize(function () {
        var tw = $(target).parent().width();
        var sw = seriesName.length * 100;
        var line = sw / tw + 1;
        var th = 400 + line * 20;

        $(target).css({'width': tw + 'px', 'height': th + 'px'});

        myChart.resize();
    });
}

//url参数
var ratingId = getUrlParam('id') ? parseInt(getUrlParam('id')) : null;

//获取url参数值
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数

    if (r != null) {
        return r[2];
    }
    else {
        return null; //返回参数值
    }
}

//datatables 中文显示设置
var datatableLngs = {
    "emptyTable": "无符合条件的数据",
    "info": "当前显示 _START_ 至 _END_, 共 _TOTAL_ 条数据",
    "infoEmpty": "",
    "infoFiltered": "(从 _MAX_ 条数据中搜索)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "每页显示 _MENU_ 条数据",
    "loadingRecords": "加载中...",
    "processing": "加载中...",
    "search": "过滤",
    "zeroRecords": "未查询到相关数据",
    "paginate": {
        "first": "首页",
        "last": "末页",
        "next": "下一页",
        "previous": "上一页"
    },
    "aria": {
        "sortAscending": ": 从小到大排序",
        "sortDescending": ": 从大到小排序"
    }
};


$.getJSON = function (url, data, callback, errorback) {
    openLoading();

    $.ajax({
        url: ajaxUrl + url + "&UserId=" + $.cookie('custId'),
        type: "get",
        contentType: "application/json",
        dataType: "json",
        timeout: 10000,
        data: data,
        // crossDomain: true ,
        crossDomain: true == !(document.all),
        success: function (data) {
            closeLoading();
            callback(data);
        },
        error: function (xhr, textstatus, thrown) {
            closeLoading();
            errorback()
        }
    });
};
$.getJsonNoUid = function (url, data, callback, errorback) {
    openLoading();

    $.ajax({
        url: ajaxUrl + url,
        type: "get",
        contentType: "application/json",
        dataType: "json",
        timeout: 10000,
        data: data,
        // crossDomain: true ,
        crossDomain: true == !(document.all),
        success: function (data) {
            closeLoading();
            callback(data);
        },
        error: function (xhr, textstatus, thrown) {
            closeLoading();
            errorback()
        }
    });
};
/**
 * 提交json数据的post请求
 * @author laixm
 */
$.postJSON = function (url, data, callback, errorback) {
    data.UserId = $.cookie("custId");
    openLoading();
    $.ajax({
        type: "post",
        method: 'post',
        url: ajaxUrl + url,
        dataType: 'json',
        crossDomain: true,
        contentType: 'application/json',
        cache: false,
        data: data,
        timeout: 60000,
        success: function (msg) {
            closeLoading();
            callback(msg);
        },
        error: function (xhr, textstatus, thrown) {
            closeLoading();
            errorback()
        }
    });
};

function alertModal(title, content) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            ok: {
                text: "确定",
                btnClass: 'btn-primary',
                keys: ['enter']
            }
        }
    })

}

function alertAndReload(title, content) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            confirm: {
                text: '确定',
                action: function () {
                    window.location.reload();
                }
            }
        }
    })
}

function alertModalyu(title, content) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            ok: {
                text: "确定",
                btnClass: 'btn-primary',
                keys: ['enter']
            }
        }
    })

    $.postJSON('/fee/student/query', JSON.stringify(data), callback, errorback)
}
