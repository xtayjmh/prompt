var originalUsername = '';

function searchHeader() {
    var user = JSON.parse($.cookie('user'));
    $("#username-header").val(user.userName);
    $("#name-header").val(user.custName);
    $("#role-header").val(user.orole.RoleName);
    $("#phone-header").val(user.custMobile);
    $("#password-header1").val(user.custPwd)
    $("#password-header").val(user.custPwd)
    $("#hdn-id").val(user.custId)
    $("#hdn-role").val(user.orole);
}

function saveHeader() {
    if ($('#demo-form-header').parsley().isValid()) {
        var password = $('#password-header').val(); //Pwd
        var username = $('#username-header').val(); //用户名
        var name = $('#name-header').val(); //姓名
        var phone = $('#phone-header').val(); //电话号码
        var jsonstr = {
            "userName": username,
            "custName": name,
            "custMobile": phone,
            "custPwd": password,
            "custId": $("#hdn-id").val(),
            "orole":$("#hdn-role").val()
        }
        var expiresDate = new Date();
        expiresDate.setTime(expiresDate.getTime() + (90 * 60 * 1000));
        $.postJSON("/RoleManage/UpdateUser", JSON.stringify(jsonstr), function (data) {
                if (data.resultCode == 'SUCCESS') {
                    $("#modal-form-heard").modal("hide");
                    $("#modal-heard").modal("show");
                    $.cookie('loginName', name, {
                        expires: expiresDate
                    });
                    $.cookie('pw', password, {
                        expires: expiresDate
                    });
                    $('#span-account').empty().append(name);
                    $.cookie('user', JSON.stringify(jsonstr), {expires: expiresDate});
                    closeLoading();
                } else {
                    $("#modal-form-heard").modal("hide");
                    alert(data.resultMsg);
                    closeLoading();
                }
            }, function (jqXHR, textStatus, errorThrown) {
                alert('修改用户失败');
                closeLoading();
            }
        );
        return false;
    }
    return true;
}
